import Vue from "vue";
import Router from "vue-router";

/** Views */
import Home from "./views/mainBoard.vue";
import Board from "./views/board.vue"

Vue.use(Router);

const router = new Router({
    routes: [{
        path: "/",
        name: "home",
        component: Home
    },
    {
        path: "/board/:id",
        name: "board",
        component: Board
    }]
})

export default router;